import cv2
import numpy as np
import glob

pic_conut = 0

def nothing(x):
    pass

cv2.namedWindow('image')

# 创建两个滑块
cv2.createTrackbar('brightness', 'image', 0, 100, nothing)
cv2.createTrackbar('contrast', 'image', 100, 200, nothing)

cv2.waitKey(0)  # 是否確認

for img in glob.glob("../python_opencv_draw/pictures/*.jpg"):
    n = cv2.imread(img)
    pic_conut = pic_conut + 1
    print(pic_conut)  # 計算圖片數量
    print(img)  # 顯示圖片路徑

    # 得到两个滑块的值
    brightness = cv2.getTrackbarPos('brightness', 'image')
    contrast = cv2.getTrackbarPos('contrast', 'image') * 0.01

    # 进行对比度和亮度调整
    temp = np.uint8(np.clip(contrast * n + brightness, 0, 255))
    cv2.imshow('image', temp)  # 重新繪圖調整過的畫面

    # cv2.waitKey(0) #是否確認
    print("save")
    # 檔名結構
    filename = "./output" + str(pic_conut) + "_" + str(brightness) + "_" + str(contrast) + ".jpg"
    print(filename)  # 輸出檔名

    cv2.imwrite(filename, temp)  # 存檔

cv2.destroyAllWindows()
