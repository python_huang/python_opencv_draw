import cv2
import numpy as np

drawing = False # true if mouse is pressed
ix,iy = -1,-1

def nothing(x):
    pass

# 滑鼠功能
def onmouse(event,x,y,flags,param):
    global ix,iy,drawing
    g = param[0]
    b = param[1]
    r = param[2]
    shape = param[3]
    # 繪圖
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            if shape == 1:
                cv2.circle(img, (x, y), 3, (g, b, r), -1)
                cv2.line(img, (ix,iy), (x,y),(g,b,r),3)
                ix = x
                iy = y
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if shape == 0:
            cv2.rectangle(img, (ix, iy), (x, y), (g, b, r), -1)
    # 換顏色
    elif event == cv2.EVENT_LBUTTONDBLCLK:
        print("LBUTTONDBLCLK")
        mouse_temp = img[y, x]
        b = cv2.setTrackbarPos('B','image',mouse_temp[0])
        g = cv2.setTrackbarPos('G','image',mouse_temp[1])
        r = cv2.setTrackbarPos('R','image',mouse_temp[2])


# 畫布
img = np.zeros((650,1000,3), np.uint8)
cv2.namedWindow('image')

# 顏色滑條
cv2.createTrackbar('R','image',0,255,nothing)
cv2.createTrackbar('G','image',0,255,nothing)
cv2.createTrackbar('B','image',0,255,nothing)

# 開始/矩形線條開關
switch1 = '0 : OFF \n1 : ON'
switch2 = '0: Rectangle \n1: Line '
cv2.createTrackbar(switch1, 'image',0,1,nothing)
cv2.createTrackbar(switch2, 'image',0,1,nothing)

while(1):
    cv2.imshow('image', img)
    k = cv2.waitKey(1) & 0xFF
    # get current positions of four trackbars
    if k == 27:
            break
    # 顏色/開始/矩形線條 取得拉條的值
    r = cv2.getTrackbarPos('R','image')
    g = cv2.getTrackbarPos('G','image')
    b = cv2.getTrackbarPos('B','image')
    shape = cv2.getTrackbarPos(switch2,'image')
    s = cv2.getTrackbarPos(switch1,'image')
    # 調色盤
    b1 = [0, 0, 255, 0, 255, 255, 255, b]
    g1 = [0, 255, 0, 255, 0, 255, 255, g]
    r1 = [255, 0, 0, 255, 255, 0, 255, r]
    for i in range(0, 8):
        paint = cv2.rectangle(img, (20 + (i-1)*20, 0), (20 + i*20, 20), (b1[i] ,g1[i] ,r1[i]), -1)
    if s == 0:
        img[:] = 0
    else:
        if k == 27:
            break
        cv2.setMouseCallback('image', onmouse, (b, g, r, shape))

cv2.waitKey(0)
cv2.destroyAllWindows()