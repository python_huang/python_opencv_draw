import numpy as np
import cv2

img = np.zeros((256, 256, 3), np.uint8)
#img.fill(200)

#cv2.line(img, (0, 0), (255, 255), (0, 0, 255), 5)
x_size=40
y_size=40


# 綠色實心方框
cv2.rectangle(img, (0, y_size), (x_size, y_size+y_size), (0, 255, 0), -1)
cv2.rectangle(img, (0, y_size*2), (x_size, y_size*2+y_size), (255, 0, 0), -1)
cv2.rectangle(img, (0, y_size*3), (x_size, y_size*3+y_size), (0, 0, 255), -1)


#cv2.imshow('My Image', img)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
def onmouse(event,x,y,flags,param):
    if event==cv2.EVENT_MOUSEMOVE:
        print(x,y)
        print(img[y,x])

def main():
    cv2.namedWindow("img")
    cv2.setMouseCallback("img",onmouse)
    while True:
        cv2.imshow("img",img)
        if cv2.waitKey()==ord('q'):break
    cv2.destroyAllWindows()

if __name__ =='__main__':
    main()
