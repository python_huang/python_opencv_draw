import cv2
import numpy as np
import glob

count = 0

def nothing(x):
    pass

cv2.namedWindow('image')

# 创建两个滑块
cv2.createTrackbar('brightness', 'image', 0, 100, nothing)
cv2.createTrackbar('contrast', 'image', 100, 200, nothing)
cv2.waitKey(0)

file = open('list.txt', 'w')
for img in glob.iglob(r'../python_opencv_draw/pictures/*.jpg'):
# iglob基本跟glob一樣,路徑前的r是防止字符轉義
    n = cv2.imread(img)
    file.write(img)
    file.write('\n')
file.close()
# list存檔與關閉不在for循環理,會儲存所有路徑,之後再執行也是相同內容的覆蓋

while count < 3:
    file = open('list.txt', 'r')

    content = file.readlines()
    img = content[count]
    img = img[:-1]
    # [:-1]刪除倒數第1個字符,在此\n被刪除
    k = cv2.imread(img)

    print(img)  # 顯示圖片路徑
    # 得到两个滑块的值
    brightness = cv2.getTrackbarPos('brightness', 'image')
    contrast = cv2.getTrackbarPos('contrast', 'image') * 0.01

    # 进行对比度和亮度调整
    temp = np.uint8(np.clip(contrast * k + brightness, 0, 255))
    cv2.imshow('image', temp)  # 重新繪圖調整過的畫面

    print("save")
    filename = "./changed" + str(count) + "_" + str(brightness) + "_" + str(contrast) + ".jpg"
    cv2.imwrite(filename, temp)
    print(filename)  # 輸出檔名

    count = count + 1
    print(count)  # 計算圖片數量

cv2.destroyAllWindows()